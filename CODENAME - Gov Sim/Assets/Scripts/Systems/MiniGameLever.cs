using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MiniGameLever : MonoBehaviour
{
    [Header("Mini Game Peramaters")]
    [SerializeField] Vector3 minPosition;
    [SerializeField] Vector3 maxPosition;

    [SerializeField] GameObject leverGame;

    [SerializeField] Vector3 LeverPos;

    [SerializeField] bool transformX;
    [SerializeField] bool transformY;
    [SerializeField] bool transformZ;

    float percentDist = 0;

    [Range(0, 100)]
    [SerializeField] float percentDistTest;

    float percentDistTestMin;
    float percentDistTestMax;
    [SerializeField] float playArea = 10;

    Vector3 LeverPosTest1 = new Vector3(0, 0, 0);
    Vector3 LeverPosTest2 = new Vector3(0, 0, 0);

    #region game

    bool isGameActive = false;

    float randomPercentage;

    [SerializeField] TMP_Text percentageUI;

    [Header("Game Clarity")]

    [SerializeField] GameObject gameObjectActive;

    [SerializeField] GameObject miniGameRight;
    [SerializeField] GameObject miniGameWrong;

    #endregion

    private void Start()
    {
        LeverPos = leverGame.transform.position;

    }

    // Game Setup Or Game Countdown

    IEnumerator Countdown()
    {
        float timer = Random.Range(20, 25);
        

        Debug.Log($"Timer: {timer}");
        yield return new WaitForSeconds(timer);

        Debug.Log($"Game Active");
        gameObjectActive.SetActive(true);
        
        isGameActive = true;

        randomPercentage = Mathf.RoundToInt(Random.Range(0f, 100f));
        percentDistTest = randomPercentage;
        percentageUI.text = "Get: " + randomPercentage.ToString();



        Setup();
        yield return null;
    }

    // Game Calculation Systems
    [ContextMenu("Setup Percentage")]
    void Setup()
    {

        if ((percentDistTest - playArea) >= 0)
        {
            Debug.Log("Higher than 0");
            percentDistTestMin = percentDistTest - playArea;
            Debug.Log(percentDistTestMin);
        }
        else
        {
            percentDistTestMin = 0f;
            Debug.Log("Lower than 0");
            Debug.Log(percentDistTestMin);
        }

        if ((percentDistTest + playArea) <= 100)
        {
            Debug.Log("Lower than 100");
            percentDistTestMax = percentDistTest + playArea;
            Debug.Log(percentDistTestMax);
        }
        else
        {
            percentDistTestMax = 100f;
            Debug.Log("Higher than 100");
            Debug.Log(percentDistTestMax);
        }

    }

    [ContextMenu("Test Percentage")] // how to get a % from a pos
    void TestPercentage()
    {
        LeverPos = leverGame.transform.position;

        if (transformX)
        {
            percentDist = (LeverPos.x - minPosition.x) / (maxPosition.x - minPosition.x) * 100;
        }
        else if (transformY)
        {
            percentDist = (LeverPos.y - minPosition.y) / (maxPosition.y - minPosition.y) * 100;
        }
        else if (transformZ)
        {
            percentDist = (LeverPos.z - minPosition.z) / (maxPosition.z - minPosition.z) * 100;
        }

        Debug.Log($"{percentDist}");
    }

    [ContextMenu("Test Reverse Pos")] //how to get a pos from a %
    void ReversePosition()
    {
        LeverPos = leverGame.transform.position;
        if (transformX)
        {
            Vector3 LeverPosTest = new Vector3(0, 0, 0);
            LeverPosTest.x = percentDistTest / 100 * (maxPosition.x - minPosition.x) + minPosition.x;

            Debug.Log($"Lever Pos Test Reverse Test X: {LeverPosTest.x}");

        }
        else if (transformY)
        {

            Vector3 LeverPosTest = new Vector3(0, 0, 0);
            LeverPosTest.y = percentDistTest / 100 * (maxPosition.y - minPosition.y) + minPosition.y;

            Debug.Log($"Lever Pos Test Reverse Test Y: {LeverPosTest.y}");
        }
        else if (transformZ)
        {

            Vector3 LeverPosTest = new Vector3(0, 0, 0);
            LeverPosTest.z = percentDistTest / 100 * (maxPosition.z - minPosition.z) + minPosition.z;

            Debug.Log($"Lever Pos Test Reverse Test Z: {LeverPosTest.z}");

        }
    }

    [ContextMenu("Test if lever is between 2 % points")]
    bool TestLeverPosInRange()
    {
        LeverPos = leverGame.transform.position;

        if (transformX)
        {

            LeverPosTest2.x = percentDistTestMax / 100 * (maxPosition.x - minPosition.x) + minPosition.x;
            LeverPosTest1.x = percentDistTestMin / 100 * (maxPosition.x - minPosition.x) + minPosition.x;

            if ((LeverPosTest1.x < Vector3.zero.x) && (LeverPosTest2.x < Vector3.zero.x))
            {

                if ((LeverPos.x <= LeverPosTest1.x) && (LeverPos.x >= LeverPosTest2.x))
                {
                    Debug.Log($"Lever Pos Test in Percentage Test X: Lever is between Max {LeverPosTest2.x} , Min {LeverPosTest1.x}");
                    return true;
                }
                else
                {
                    Debug.Log($"Lever isnt close to position between two point in Test X ");
                    return false;
                    //Debug.Log($"{LeverPosTest1.x}");
                    //Debug.Log($"{LeverPosTest2.x}");
                }

            }
            else
            {
                if ((LeverPos.z >= LeverPosTest1.z) && (LeverPos.x <= LeverPosTest2.x))
                {
                    Debug.Log($"Lever Pos Test in Percentage Test X: Lever is between Max {LeverPosTest2.x} , Min {LeverPosTest1.x}");
                    return true;
                }
                else
                {
                    Debug.Log($"Lever isnt close to position between two point in Test X ");
                    return false;
                    //Debug.Log($"{LeverPosTest1.x}");
                    //Debug.Log($"{LeverPosTest2.x}");
                }

            }

        }
        else if (transformY)
        {

            
            LeverPosTest2.y = percentDistTestMax / 100 * (maxPosition.z - minPosition.y) + minPosition.y;
            LeverPosTest1.y = percentDistTestMin / 100 * (maxPosition.z - minPosition.y) + minPosition.y;

            if ((LeverPosTest1.y < Vector3.zero.y) && (LeverPosTest2.y < Vector3.zero.y))
            {

                if ((LeverPos.y <= LeverPosTest1.y) && (LeverPos.y >= LeverPosTest2.y))
                {
                    Debug.Log($"Lever Pos Test in Percentage Test Y: Lever is between Max {LeverPosTest2.z} , Min {LeverPosTest1.z}");
                    return true;
                }
                else
                {
                    Debug.Log($"Lever isnt close to position between two point in Test Y");
                    return false;
                    //Debug.Log($"{LeverPosTest1.y}");
                    //Debug.Log($"{LeverPosTest2.y}");
                }

            }
            else
            {
                if ((LeverPos.y >= LeverPosTest1.y) && (LeverPos.y <= LeverPosTest2.y))
                {
                    Debug.Log($"Lever Pos Test in Percentage Test Y: Lever is between Max {LeverPosTest2.y} , Min {LeverPosTest1.y}");
                    return true;
                }
                else
                {
                    Debug.Log($"Lever isnt close to position between two point in Test Y");
                    return false;
                    //Debug.Log($"{LeverPosTest1.y}");
                    //Debug.Log($"{LeverPosTest2.y}");
                }

            }
        }
        else if (transformZ)
        {

            LeverPosTest2.z = percentDistTestMax / 100 * (maxPosition.z - minPosition.z) + minPosition.z;
            LeverPosTest1.z = percentDistTestMin / 100 * (maxPosition.z - minPosition.z) + minPosition.z;

            if ((LeverPosTest1.z < Vector3.zero.z) && (LeverPosTest2.z < Vector3.zero.z))
            {

                if ((LeverPos.z <= LeverPosTest1.z) && (LeverPos.z >= LeverPosTest2.z))
                {
                    Debug.Log($"Lever Pos Test in Percentage Test Z: Lever is between Max {LeverPosTest2.z} , Min {LeverPosTest1.z}");
                    return true;
                }
                else
                {
                    Debug.Log($"Lever isnt close to position between two point in Test Z ");
                    //Debug.Log($"{LeverPosTest1.z}");
                    //Debug.Log($"{LeverPosTest2.z}");
                    return false;
                }

            }
            else
            {
                if ((LeverPos.z >= LeverPosTest1.z) && (LeverPos.z <= LeverPosTest2.z))
                {
                    Debug.Log($"Lever Pos Test in Percentage Test Z: Lever is between Max {LeverPosTest2.z} , Min {LeverPosTest1.z}");
                    return true;
                }
                else
                {
                    Debug.Log($"Lever isnt close to position between two point in Test Z ");
                    return false;
                    //Debug.Log($"{LeverPosTest1.z}");
                    //Debug.Log($"{LeverPosTest2.z}");

                }

            }

        }
        return false;
    }


    // Button Nonsense

    public void EnterLeverInput()
    {
        Debug.Log($"Button pressed for input");


        if (isGameActive)
        {
            isGameActive = false;
            gameObjectActive.SetActive(false);
            StartCoroutine(Countdown());

            if (TestLeverPosInRange())
            {
                // add water here
                StartCoroutine(ShowRight());
                Debug.Log($"Add Water Here and happiness");
                CountryHappinessSystem.Instance.AddHappy(180);

            }
            else
            {
                // Remove water here
                StartCoroutine(ShowWrong());
                CountryHappinessSystem.Instance.RemoveHappy(180);
                Debug.Log($"Remove Water Here and happiness");
            }

        }
    }

    IEnumerator ShowRight(){
        miniGameRight.SetActive(true);
        yield return new WaitForSeconds(3f);
        miniGameRight.SetActive(false);
    }

    IEnumerator ShowWrong(){
        miniGameWrong.SetActive(true);
        yield return new WaitForSeconds(3f);
        miniGameWrong.SetActive(false);
    }

    //Game Stuff

    public void StartLeverGame()
    {   
        StartCoroutine(Countdown());
        //and ui text here
    }
    
}
