using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;

public class WaterSystem : MonoBehaviour
{
    public static WaterSystem Instance;

    void Awake()
    {
        Instance = this;
    }


    #region Variables

    [Header("Essential Numbers")]
    [Space(3)]
    [SerializeField] public float currentWater;
    [Space(3)]
    [SerializeField] float extremeMaxWater = 5000;
    [Space(3)]
    [SerializeField] float maxWater = 4000;
    [Space(3)]
    [SerializeField] float minWater = 1000;
    [Space(3)]
    [SerializeField] float extremeMinWater = 100;

    [Space(30)]
    [Header("What will consume Power?")]
    [Space(3)]
    [SerializeField] float waterConsumptionUnits;

    [Space(30)]
    [Header("UI Elements")]
    [Space(3)]
    [SerializeField] TMP_Text currentWaterText;
    [Space(3)]
    [SerializeField] Image currentWaterTankUI;
    [SerializeField] Color badColour;
    [SerializeField] Color okColour;

    [SerializeField] public bool waterActive = false;

 

    #endregion


    void ChangeWaterEvent()
    {
        Debug.Log($"Event is firing off");
        ChangeWater();
    }

    void Start()
    {
        currentWater = Mathf.Round((minWater + maxWater) / 2);
        waterConsumptionUnits = 2;
        currentWaterText.text = currentWater.ToString();

        currentWaterTankUI.fillAmount = currentWater / extremeMaxWater;
        currentWaterTankUI.color = okColour;

        StartCoroutine(GameStatsWater());

    }

    IEnumerator GameStatsWater()
    {

        while (true)
        {
            float totalTime = 0;
            while (totalTime <= 3)
            {
                totalTime += Time.deltaTime;
                var integer = (int)totalTime; /* choose how to quantize this */
                yield return null;
            }

            currentWaterText.text = currentWater.ToString();

            currentWaterTankUI.fillAmount = currentWater / extremeMaxWater;

            if (currentWater <= minWater)
            {
                currentWaterTankUI.color = badColour;
            }
            else if (currentWater >= maxWater)
            {
                currentWaterTankUI.color = badColour;
            }
            else
            {
                currentWaterTankUI.color = okColour;
            }


        }
    }

    [ContextMenu("Start Game")]
    public void StartGame()
    {
        StartCoroutine(PowerAddSubt(waterConsumptionUnits));
    }


    IEnumerator PowerAddSubt(float waterEaters)
    {
        float totalTime = 0;
        while (totalTime <= 3)
        {

            totalTime += Time.deltaTime;
            var integer = (int)totalTime;


            yield return null;
        }

        if (waterActive)
        {
            //Debug.Log($"Removing Water");
            currentWater -= 100 + (waterEaters * 10);
            RestartWater();
            //Debug.Log($"New Water: {currentWater}");
        }
        else
        {
            //Debug.Log($"Add Water");
            currentWater += 100 + (waterEaters * 10);
            RestartWater();
            //Debug.Log($"New Water: {currentWater}");
        }
    }

    void RestartWater()
    {
        StartCoroutine(PowerAddSubt(waterConsumptionUnits));
    }


    [ContextMenu("PowerDeactivate")]
    public void ChangeWater()
    {
        if (waterActive)
        {
            waterActive = false;
        }
        else
        {
            waterActive = true;
        }
    }

    public void AddWaterUnit()
    {
        if (waterConsumptionUnits < 10)
        {
            waterConsumptionUnits++;

        }

    }

    public void RemoveWaterUnit()
    {
        if (waterConsumptionUnits > 1)
        {
            waterConsumptionUnits--;
        }
    }

}
