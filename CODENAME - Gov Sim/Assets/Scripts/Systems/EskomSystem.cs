using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;

public class EskomSystem : MonoBehaviour
{
    public static EskomSystem Instance;

    void Awake()
    {
        Instance = this;
    }

    #region Variables

    [Header("Essential Numbers")]
    [Space(3)]
    [SerializeField] public float currentPower;
    [Space(3)]
    [SerializeField] float extremeMaxPower = 3800;
    [Space(3)]
    [SerializeField] float maxPower = 3200;
    [Space(3)]
    [SerializeField] float minPower = 1600;
    [Space(3)]
    [SerializeField] float extremeMinPower = 1000;

    [Space(30)]
    [Header("What will consume Power?")]
    [Space(3)]
    [SerializeField] float powerConsumptionUnits;

    [Space(30)]
    [Header("UI Elements")]
    [Space(3)]
    [SerializeField] TMP_Text currentPowerText;
    [Space(3)]
    [SerializeField] TMP_Text currentPowerUnitText;
    [Space(3)]
    [SerializeField] Image currentPowerTankUI;
    [SerializeField] Color badColour;
    [SerializeField] Color okColour;

    [SerializeField] public bool powerActive = false;

    #endregion

    void Start()
    {
        currentPower = Mathf.Round((minPower + maxPower)/2);
        powerConsumptionUnits = 2;

        currentPowerText.text = currentPower.ToString();
        currentPowerUnitText.text = powerConsumptionUnits.ToString();


        currentPowerTankUI.fillAmount = currentPower / extremeMaxPower;

        StartCoroutine(GameStatsPower());

    }

    [ContextMenu("Start Game")]
    public void StartGame()
    {
        StartCoroutine(PowerAddSubt(powerConsumptionUnits));
    }

    IEnumerator GameStatsPower()
    {

        while (true)
        {
            float totalTime = 0;
            while (totalTime <= 3)
            {
                totalTime += Time.deltaTime;
                var integer = (int)totalTime; /* choose how to quantize this */
                yield return null;
            }

            currentPowerText.text = currentPower.ToString();

            currentPowerTankUI.fillAmount = currentPower / extremeMaxPower;

            if (currentPower <= minPower)
            {
                currentPowerTankUI.color = badColour;
            }
            else if (currentPower >= maxPower)
            {
                currentPowerTankUI.color = badColour;
            }
            else
            {
                currentPowerTankUI.color = okColour;
            }
        }
    }


    IEnumerator PowerAddSubt(float powerEaters)
    {
        float totalTime = 0;
        while (totalTime <= 3)
        {
            //countdownImage.fillAmount = totalTime / duration;
            totalTime += Time.deltaTime;
            var integer = (int)totalTime; /* choose how to quantize this */
            /* convert integer to string and assign to text */
            ////Debug.Log($"Timer : {integer}");
            yield return null;
        }

        if (powerActive)
        {
            currentPower -= 100 + (powerEaters * 10);
            RestartPower();
            //Debug.Log($"Removing Power, New CurrentPower: {currentPower}");
        }
        else
        {
            currentPower += 100 + (powerEaters * 10);
            RestartPower();
            //Debug.Log($"Add Power, New CurrentPower: {currentPower}");
        }
    }

    void RestartPower()
    {
        StartCoroutine(PowerAddSubt(powerConsumptionUnits));
    }


    [ContextMenu("PowerDeactivate")]
    public void ChangePower()
    {
        if (powerActive)
        {
            powerActive = false;
        }
        else
        {
            powerActive = true;
        }
    }

    public void PowerOn()
    {
        if (!powerActive)
        {
            powerActive = true;
            WaterSystem.Instance.ChangeWater();

        }
    }

    public void PowerOff()
    {
        if (powerActive)
        {
            powerActive = false;
            //WaterSystem.Instance.waterActive = false;
            WaterSystem.Instance.ChangeWater();

        }
    }

    public void AddPowerUnit()
    {
        if (powerConsumptionUnits < 10)
        {
            powerConsumptionUnits++;
            currentPowerUnitText.text = powerConsumptionUnits.ToString();

        }

    }

    public void RemovePowerUnit()
    {
        if (powerConsumptionUnits > 1)
        {
            powerConsumptionUnits--;
            currentPowerUnitText.text = powerConsumptionUnits.ToString();
        }
    }


}
