using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;


public class TaxesSystem : MonoBehaviour
{
    public static TaxesSystem Instance;

    void Awake()
    {
        Instance = this;
    }

    #region Variables
    [Header("Essential Numbers")]

    [SerializeField] float taxMoney;

        [Space(30)]
    [Header("UI Elements")]
    [Space(3)]
    [SerializeField] TMP_Text currentTaxMoneyText;
    [Space(3)]
    [SerializeField] Animator warningTaxOverloadText;
    [SerializeField] Animator warningTaxShutDownText;



    #endregion


    private void OnEnable()
    {
        //EskomSystem.IncreaseTaxesWater.AddListener(IncreaseMoney);
    }

    private void OnDisable()
    {
        //EskomSystem.IncreaseTaxesWater.RemoveListener(IncreaseMoney);
    }

    void IncreaseMoney()
    {
        //Debug.Log($"Event is firing off");
    }

    


    


}
