using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SystemManager : MonoBehaviour
{

    [SerializeField] GameObject endCanvas;

    public void StartGame()
    {
        WaterSystem.Instance.ChangeWater();
        EskomSystem.Instance.ChangePower();

        WaterSystem.Instance.StartGame();
        EskomSystem.Instance.StartGame();
        CountryHappinessSystem.Instance.StartGame();

    }

    private void Update() {
        if (WaterSystem.Instance.currentWater <= 0)
        {
            //Show End Game Canvas wait two secs and restart the game
            StartCoroutine(RestartGame());
        }

        if (EskomSystem.Instance.currentPower <= 0)
        {
            //Show End Game Canvas wait two secs and restart the game
            StartCoroutine(RestartGame());
        }

        if (CountryHappinessSystem.Instance.currentHappiness <= 0)
        {
            //Show End Game Canvas wait two secs and restart the game
            StartCoroutine(RestartGame());
        }
    }

    IEnumerator RestartGame(){
        endCanvas.SetActive(true);
        yield return new WaitForSeconds(5);
        endCanvas.SetActive(false);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        yield return null;
    }
}
