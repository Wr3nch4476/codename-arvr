using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;

public class CountryHappinessSystem : MonoBehaviour
{
    public static CountryHappinessSystem Instance;
    void Awake()
    {
        Instance = this;
    }

    #region Variables

    [Header("Essential Numbers")]
    [Range(0.3f, 0.5f)]
    [SerializeField] float percentStart = 0.4f;
    [Space(3)]
    [SerializeField] public float currentHappiness;
    [Space(3)]
    [SerializeField] float extremeMaxHappiness = 3800;
    [Space(3)]
    [SerializeField] float maxHappiness = 3200;
    [Space(3)]
    [SerializeField] float minHappiness = 1600;
    [Space(3)]
    [SerializeField] float extremeMinHappiness = 1000;

    [Space(30)]
    [Header("UI Elements")]
    [Space(3)]
    [SerializeField] TMP_Text currentHappText;
    [Space(3)]
    [SerializeField] Image currentCountryHappinessUI;
    [SerializeField] Color badColour;
    [SerializeField] Color okColour;


    [SerializeField] bool happyActive = false;

    #endregion

    void Start()
    {
        currentHappiness = Mathf.Round(percentStart * maxHappiness);
        StartCoroutine(GameStatsHapp());

        currentCountryHappinessUI.fillAmount = currentHappiness / extremeMaxHappiness;
        currentCountryHappinessUI.color = okColour;
    }
    
    private void FixedUpdate()
    {
        if (WaterSystem.Instance.waterActive)
        {
            happyActive = true;
        }
        else
        {
            happyActive = false;
        }

        if ( EskomSystem.Instance.powerActive)
        {
            happyActive = true;
        }
        else
        {
            happyActive = false;
        }
    }

    IEnumerator GameStatsHapp()
    {

        while (true)
        {
            float totalTime = 0;
            while (totalTime <= 3)
            {
                totalTime += Time.deltaTime;
                var integer = (int)totalTime; /* choose how to quantize this */
                yield return null;
            }

            currentHappText.text = currentHappiness.ToString();

            currentCountryHappinessUI.fillAmount = currentHappiness / extremeMaxHappiness;

            if (currentHappiness <= minHappiness)
            {
                currentCountryHappinessUI.color = badColour;
            }
            else if (currentHappiness >= maxHappiness)
            {
                currentCountryHappinessUI.color = badColour;
            }
            else
            {
                currentCountryHappinessUI.color = okColour;
            }
        }
    }

    public void StartGame()
    {
        StartCoroutine(HappyAddSubt());

    }


    IEnumerator HappyAddSubt()
    {
        float totalTime = 0;
        while (totalTime <= 3)
        {
            //countdownImage.fillAmount = totalTime / duration;
            totalTime += Time.deltaTime;
            var integer = (int)totalTime; /* choose how to quantize this */
            /* convert integer to string and assign to text */
            //Debug.Log($"Timer : {integer}");
            yield return null;
        }

        if (!happyActive)
        {
            currentHappiness -= 100;
            RestartHapp();
        }
        else
        {
            currentHappiness += 100;
            RestartHapp();
        }
    }

    void RestartHapp()
    {
        StartCoroutine(HappyAddSubt());
    }

    public void AddHappy(float numberAdd){
        currentHappiness += numberAdd;
    }

    public void RemoveHappy(float numberRemove){
        currentHappiness -=  numberRemove;
    }

}
